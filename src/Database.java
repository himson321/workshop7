import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class Database {
    private FileWriteable[] object=new FileWriteable[100];
    private static int numobject=0;

    public Database() {
    }

    public void addobject(FileWriteable input){
        object[numobject++]=input;
    }

    public void removeobject(FileWriteable input){
        for (int n=0;n<numobject;n++){
            if (object[n].equals(input)){
                for (int j=n;j<numobject;j++){
                    object[j]=object[j+1];
                }
            }
            }
        numobject--;
    }
   public void writeAll(String filename) throws IOException {
       BufferedWriter bw =
               new BufferedWriter(new FileWriter(filename));
       for (int n=0;n<numobject;n++){
            object[n].writeToFile(bw);
        }
   }
    public static void main(String[] args) throws IOException {
        Point pointa=new Point(2.1,2.3);
        Database data=new Database();
        data.addobject(pointa);
        data.writeAll("ABC.txt");
    }
}
